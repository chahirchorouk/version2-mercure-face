import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
/*addedd */
import { CdkTableModule } from '@angular/cdk/table';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ChartsModule } from 'ng2-charts';
import { IonRangeSliderModule } from 'ng2-ion-range-slider';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { OrderModule } from 'ngx-order-pipe';
import { NgDatepickerModule } from 'ng2-datepicker';

import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';

import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { legalPersonsDataService } from './persons-services/legal-persons-data.service';
import { OpportunityDataService } from './opprtunity/opportunities-data.service';
import { naturalPersonsDataService } from './persons-services/natural-persons-data.service';
import { personsDataService } from './persons-services/persons-data.service';
import { PersonsComponent } from './persons/persons.component';
import { PersonsDetailsComponent } from './persons-details/person-details.component';
import { PagerService } from './persons-services/pager.service';
import { SearchPersonsComponent } from './search-persons/search-persons.component';
import { FilterPipe } from './persons/filter.pipe';

import { PersonEditDetailsComponent } from './person-edit-details/person-edit-details.component';
import { SuiteEditDetailsComponent } from './suite-edit-details/suite-edit-details.component';
import { ShowNotification } from './persons-services/showNotification.service';

import { OpprtunityComponent } from './opprtunity/opprtunity.component';
import { DashboardClientComponent } from './dashboard-client/dashboard-client.component';

// import { Ng2OrderModule } from 'ng2-order-pipe';
// import { ChartsModule } from 'ng2-charts';


import {
  AgmCoreModule
} from '@agm/core';
/*import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';*/

@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    ComponentsModule,
    RouterModule,
    AppRoutingModule,
    ChartsModule,
    HttpClientModule,
    IonRangeSliderModule,
    OrderModule,
    NgDatepickerModule,
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger' // set defaults here
    }),
    AgmCoreModule.forRoot({
      apiKey: 'YOUR_GOOGLE_MAPS_API_KEY'
    })
  ],
  declarations: [
    AppComponent,
    // AdminLayoutComponent,
    AppComponent,
    DashboardComponent,
    PersonsComponent,
    PersonsDetailsComponent,
    SearchPersonsComponent,
    FilterPipe,
    PersonEditDetailsComponent,
    SuiteEditDetailsComponent,
    DashboardClientComponent,
    OpprtunityComponent,

  ],
  exports: [
    // CDK
    CdkTableModule,
  ],
  providers: [legalPersonsDataService,
    naturalPersonsDataService,
    personsDataService,
    OpportunityDataService,
    PagerService,
    ShowNotification],
  bootstrap: [AppComponent],
  entryComponents: []
})
export class AppModule { }
