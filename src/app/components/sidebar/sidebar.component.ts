import {
  Component,
  OnInit
} from '@angular/core';
import {
  Location
} from '@angular/common';
import {
  NavROUTE
} from '../navBar/navbar.component';

declare const $: any;
declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}
export const ROUTES: RouteInfo[] = [{
  path: 'person-details',
  title: 'Fiche',
  icon: 'person',
  class: ''
},
{
  path: 'dashboard-client',
  title: 'Dashboard',
  icon: 'dashboard',
  class: ''
},
{
  path: 'person-edit',
  title: 'Contact',
  icon: 'import_contacts',
  class: ''
},
{
  path: 'person-profil-edit',
  title: 'Profil Details',
  icon: 'recent_actors',
  class: ''
},
{
  path: 'command-client',
  title: 'Command list',
  icon: 'location_searching',
  class: ''
},
{
  path: 'opportunity',
  title: 'Opportunity list',
  icon: 'new_releases',
  class: ''
},
  // { path: 'command-client', title: 'Request list', icon: 'report_problem', class: '' }
];
const authorizedROUTES: any[] = [{
  path: '/person-details'
},
{
  path: '/dashboard-client'
},
{
  path: '/person-edit'
},
{
  path: '/person-profil-edit'
},
{
  path: '/command-client'
},
{
  path: '/opportunity'
}
];


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];
  navMenuItems: any[];


  constructor(public location: Location, ) { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.navMenuItems = NavROUTE.filter(menuItem => menuItem);
  }
  isHome(): any {
    const self = this;
    this.navMenuItems.forEach(function (value) {
      const path = '/' + value.path;
      if (self.location.path() === path) {
        return true;
      } else {
        return false;
      }
    });
  }
  isDetails(): any {
    if (this.location.path() === '/persons') {
      return false;
    } else {
      return true;
    }
    // for (let a of authorizedROUTES) {
    //   if (a.path == this.location.path()) {
    //     //console.log('---- event ', a.path, this.location.path());
    //     return true;
    //   } else {
    //     return false;
    //   }
    // }
  }
  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  };

}
