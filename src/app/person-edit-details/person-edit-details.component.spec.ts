import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonEditDetailsComponent } from './person-edit-details.component';

describe('PersonEditDetailsComponent', () => {
  let component: PersonEditDetailsComponent;
  let fixture: ComponentFixture<PersonEditDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonEditDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonEditDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
