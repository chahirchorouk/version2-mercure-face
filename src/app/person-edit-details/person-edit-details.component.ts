import { Component, OnInit ,ViewChild } from '@angular/core';
import { personsDataService } from '../persons-services/persons-data.service';
import { legalPersonsDataService} from '../persons-services/legal-persons-data.service';
import { naturalPersonsDataService } from '../persons-services/natural-persons-data.service';
import { ShowNotification} from '../persons-services/showNotification.service';
import { Router } from '@angular/router';

import { DatepickerOptions } from 'ng2-datepicker';
import * as frLocale from 'date-fns/locale/fr';

declare var $:any;
@Component({
  selector: 'app-person-edit-details',
  templateUrl: './person-edit-details.component.html',
  styleUrls: ['./person-edit-details.component.scss']
})
export class PersonEditDetailsComponent implements OnInit {
	person: any;
  personEmails :any;

  date: Date;
  options: DatepickerOptions = {
    locale: frLocale,
    displayFormat: 'D MMMM  YYYY',
  };


  @ViewChild('itemId') itemId;
  constructor(public dataservice: personsDataService,
              private NdataService: naturalPersonsDataService,
              private LdataService: legalPersonsDataService,
              public showNotif : ShowNotification,
              private router: Router ) { }

  ngOnInit() {
  	this.person = this.dataservice.selectedPerson;
    this.date = new Date();


    if(!this.person) {
      this.showNotif.showNotification('bottom','center', 'warning', ' we will redirect you to the clients list to select an item in order to see his details')
      setTimeout(() => {
        console.log('i m inside the settime out ');
        this.router.navigateByUrl('/persons');
      }, 2000);
    }else {
      this.personEmails = this.person.emails;
    }
    console.log("====== person =======", this.person ); 
  }
  /* ngAfterViewInit() {
    var options={
            format: 'mm/dd/yyyy',
            todayHighlight: true,
            autoclose: true,
        };
      $(this.itemId.nativeElement).datepicker(options);
   }*/

  updatePerson(){
    if(this.person.personType == 'NATURAL'){
      this.NdataService.update(this.person);
      console.log('------update person natural ------',this.person)
    }else if(this.person.personType == 'LEGAL'){
      this.LdataService.update(this.person);
      console.log('------update person legal ------',this.person)
    }
  }
  

}
