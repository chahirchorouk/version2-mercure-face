import {
  Component,
  OnInit,
  OnDestroy
} from '@angular/core';
import * as $ from 'jquery';
declare var _: any;
import {
  LPerson
} from '../persons-services/LPerson';
import {
  NPerson
} from '../persons-services/NPerson';
import {
  Person
} from '../persons-services/Person';
import {
  legalPersonsDataService
} from '../persons-services/legal-persons-data.service';
import {
  naturalPersonsDataService
} from '../persons-services/natural-persons-data.service';
import {
  personsDataService
} from '../persons-services/persons-data.service';
import {
  Router
} from '@angular/router';
// import * as _ from 'underscore';

import {
  FilterPipe
} from './filter.pipe';

@Component({
  selector: 'app-persons',
  templateUrl: './persons.component.html',
  styleUrls: ['./persons.component.scss']
})
export class PersonsComponent implements OnInit, OnDestroy {
  pageSize = 10; // set the page elements size
  disabledClients: Person[];
  // nPersons: NPerson[];
  // 	lPersons: LPerson[];
  //  Persons: Person[];
  PersonsPages: Person[];
  PersonsNPages: NPerson[];
  PersonsLPages: LPerson[];
  selectedNperson: NPerson;
  selectedLperson: LPerson;
  selectedPerson: Person;
  editNprsn: NPerson;
  pager: any = {}; // pager object
  pagedItems: any[]; // paged items
  selectedTab: string;
  minCAslider = 10;
  maxCAslider = 1000000; // chiffre d'affaire range slider
  classifTypes: any;
  activityTypes: any;
  selectedClassif: any;
  opIsChecked: any;
  selectedActivity: any;
  validIsChecked: boolean;
  public popoverTitle = 'Confirm changes';
  public popoverMessage = 'are you sure you want to delete this person';
  public cancelClicked = false;

  key = 'name'; // set default//sorting
  reverse = false;
  sort(key) {
    this.key = key;
    this.reverse = !this.reverse;
  }
  constructor(private NdataService: naturalPersonsDataService,
    private LdataService: legalPersonsDataService,
    private PdataService: personsDataService,
    private router: Router) { }

  deletePerson(nperson): any {
    // this.PdataService.delete(nperson.id);
    this.disabledClients.push(nperson);
    this.setPage(1);
  }

  ngOnInit(): void {
    this.setPage(1);
    this.getCtypes();
    this.getActivities();
  }

  ngOnDestroy() { }

  onSelectP(cust: Person): void {
    this.selectedPerson = cust;
    this.PdataService.selectedPerson = cust;
    this.router.navigateByUrl('/person-details');
  }

  setPage(page: number) {
    // tslint:disable-next-line:no-var-keyword
    var currentPage = 1;
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    if (this.pager.currentPage !== undefined) {
      currentPage = page;
      console.log('--------- current -----------: ', currentPage)
    }

    if (this.selectedTab === 'p' || this.selectedTab === undefined) {
      this.PdataService.getPersonsPerPages(page - 1, this.pageSize).then(personsInf => {
        this.pager = personsInf;
        this.pager.currentPage = currentPage;
        this.pager.pages = Array(personsInf.totalPages).fill(1);
        this.PersonsPages = personsInf.content;
        console.log('pager', this.pager, 'pagerAllItems', this.PersonsPages);
      });
    } else if (this.selectedTab === 'n') {
      this.NdataService.getNPersonsPerPages(page - 1, this.pageSize).then(personsInf => {
        this.pager = personsInf;
        this.pager.currentPage = currentPage;
        this.pager.pages = Array(personsInf.totalPages).fill(1);
        this.PersonsNPages = personsInf.content;
        console.log('pager', this.pager, 'pagerNaturalItems', this.PersonsNPages);
      });
    } else if (this.selectedTab === 'l') {
      this.LdataService.getLPersonsPerPages(page - 1, this.pageSize).then(personsInf => {
        this.pager = personsInf;
        this.pager.currentPage = currentPage;
        this.pager.pages = Array(personsInf.totalPages).fill(1);
        this.PersonsLPages = personsInf.content;
        console.log('pager', this.pager, 'pagerLegalItems', this.PersonsLPages);
      });
    } else if (this.selectedTab === 'c') {
      this.PdataService.getPersonsByClassif(this.selectedClassif, page - 1, this.pageSize).then(personsInf => {
        this.pager = personsInf;
        this.pager.currentPage = currentPage;
        this.pager.pages = Array(personsInf.totalPages).fill(1);
        this.PersonsPages = personsInf.content;
        console.log('pager', this.pager, 'pagerAllItems', this.PersonsPages);
      });
    } else if (this.selectedTab === 'ca') {
      this.LdataService.getLPersonsPerCapital(this.minCAslider, this.maxCAslider, page - 1, this.pageSize).then(personsInf => {
        this.pager = personsInf;
        this.pager.currentPage = currentPage;
        this.pager.pages = Array(personsInf.totalPages).fill(1);
        this.PersonsPages = personsInf.content;
        console.log('pager', this.pager, 'pagerAllItems', this.PersonsPages);
      });
    } else if (this.selectedTab === 'ac') {
      this.PdataService.getPersonsByActivity(this.selectedActivity, page - 1, this.pageSize).then(personsInf => {
        this.pager = personsInf;
        this.pager.currentPage = currentPage;
        this.pager.pages = Array(personsInf.totalPages).fill(1);
        this.PersonsPages = personsInf.content;
        console.log('pager', this.pager, 'pagerAllItems', this.PersonsPages);
      });

    } else if (this.selectedTab === 'v') {
      console.log('::::::::: vallid :::::::::', this.validIsChecked);
      this.PdataService.getPersonsValid(this.validIsChecked, page - 1, this.pageSize).then(personsInf => {
        this.pager = personsInf;
        this.pager.currentPage = currentPage;
        this.pager.pages = Array(personsInf.totalPages).fill(1);
        this.PersonsPages = personsInf.content;
        console.log('pager', this.pager, 'pagerAllItems', this.PersonsPages);
      });
    } else if (this.selectedTab === 'o') {
      this.PdataService.getPersonsByOppor(page - 1, this.pageSize).then(personsInf => {
        this.pager = personsInf;
        this.pager.currentPage = currentPage;
        this.pager.pages = Array(personsInf.totalPages).fill(1);
        if (personsInf.content) {
          this.PersonsPages = personsInf.content;
        } else {
          this.PersonsPages = personsInf;
        }
        console.log('pager', this.pager, 'pagerAllItems', this.PersonsPages);
      });
    }
  }
  // when we switch between the tabs or when we click on classif advanced search
  changeNavTab(tabType: string) {
    this.selectedTab = tabType;
    this.setPage(1);
    console.log('the tab is changed ', this.selectedTab);
  }
  // to display a model not yet set
  openEditNPDialog(cust: NPerson) {
    this.editNprsn = cust;
    $('#modalTrigger').click();
  }
  // to close a model not yet set
  closeModel() {
    $('#postJobModalClose').click();
    this.editNprsn = null;
  }
  // when we specify the range of capital on advanced serach
  sliderOnFinish($event) {
    this.maxCAslider = $event.from;
    this.maxCAslider = $event.to;
    this.changeNavTab('ca');
  }
  // fill the  list of classification from th eserver
  getCtypes() {
    this.PdataService.getClassificationTypes().then(persons => {
      this.classifTypes = persons;
    })
  }
  // get list of activities from the server
  getActivities() {
    this.PdataService.getActivityTypes().then(persons => {
      this.activityTypes = persons;
    })
  }
  // when we choose a classification from advanced search
  onInputClassif($event) {
    this.selectedClassif = $event.target.value;
    this.changeNavTab('c');
  }
  // when we choose an activity from advanced search
  onInputActivity($event) {
    this.selectedActivity = $event.target.value;
    this.changeNavTab('ac');
  }
  // when we click to the opportunity checkbox in advanced search
  opCheckBox($event) {
    this.opIsChecked = $event.target.checked;
    if (this.opIsChecked === true) {
      this.changeNavTab('o');
    }
  }
  validCheckBox($event) {
    this.validIsChecked = $event.target.checked;
    // if(this.validIsChecked == true){
    this.changeNavTab('v');
    // }
  }

}
