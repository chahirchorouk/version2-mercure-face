import { Component, OnInit } from '@angular/core';
import { Person } from '../persons-services/Person';
import { personsDataService } from '../persons-services/persons-data.service';
import { legalPersonsDataService} from '../persons-services/legal-persons-data.service';
import { naturalPersonsDataService } from '../persons-services/natural-persons-data.service';
import { Location } from '@angular/common';
import { ShowNotification} from '../persons-services/showNotification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-suite-edit-details',
  templateUrl: './suite-edit-details.component.html',
  styleUrls: ['./suite-edit-details.component.scss']
})
export class SuiteEditDetailsComponent implements OnInit {

person: any;
personTypes:any ; 
classifTypes : any ;
legalForm : any ; 
activities : any ;
selectedType : any ; 
selectedClassif : any ; 
  constructor(public dataservice: personsDataService,    
              private NdataService: naturalPersonsDataService,
              private LdataService: legalPersonsDataService,
              public location: Location,
              public showNotif : ShowNotification,
              private router: Router) { }

  ngOnInit() {
  	this.person = this.dataservice.selectedPerson;
    
    if(!this.person) {
      this.showNotif.showNotification('bottom','center', 'warning', 'we will redirect you to the clients list to select an item in order to see his details')
      setTimeout(() => {
        console.log('i m inside the setTime out ');
        this.router.navigateByUrl('/persons');
      }, 2000);
    }else {
      this.getCtypes();this.getPtypes();this.getLForm();this.getActivities();
      this.selectedClassif = this.person.classification[0].type; 
      /*if ($("#typeTrigger").hasClass('is-empty')) {
          $('#typeTrigger"').removeClass('is-empty');
          console.log('ha l"arrrrrrrrrrrrrrrrrr');
      }   */  
    }
  }

  getPtypes(){
    this.dataservice.getPersonTypes().then(persons => {this.personTypes = persons;})
  }
  getCtypes(){
    this.dataservice.getClassificationTypes().then(persons => {this.classifTypes = persons;})
  }
  getLForm(){
    this.dataservice.getLegalForm().then(persons => {this.legalForm = persons;})
  }
  getActivities(){
    this.dataservice.getActivityTypes().then(persons => {this.activities = persons;})
  }

  onChangeClassif(city) {
    console.log("the select binding",city);
    var self = this; 
    this.person.classification.forEach(function (item, index, arr) {
        if (item.type === self.selectedClassif) {
            arr.splice(index, 1);
            console.log('that s it the element to remove is ',self.person.classification);
        }
    });
  }
  updatePerson() {
    if(this.person.personType == 'NATURAL'){
      this.NdataService.update(this.person);
      console.log('------update person natural ------',this.person)
    }else if(this.person.personType == 'LEGAL'){
      this.LdataService.update(this.person);
      console.log('------update person legal ------',this.person)
    }
  }
  addClassif(topic){
    this.person.classification.push({type: topic});
    console.log("the new array  ", this.person.classification );

  }
  onInput($event) {
    //$event.preventDefault();
    console.log('selected: ' + $event.target.value);
  }



}
