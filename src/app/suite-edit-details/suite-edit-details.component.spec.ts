import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuiteEditDetailsComponent } from './suite-edit-details.component';

describe('SuiteEditDetailsComponent', () => {
  let component: SuiteEditDetailsComponent;
  let fixture: ComponentFixture<SuiteEditDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuiteEditDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuiteEditDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
