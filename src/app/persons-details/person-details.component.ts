import { Component, OnInit, OnDestroy } from '@angular/core';
import { Person } from '../persons-services/Person';
import { personsDataService } from '../persons-services/persons-data.service';
import { Location } from '@angular/common';
import { ShowNotification} from '../persons-services/showNotification.service';
import { Router } from '@angular/router';

declare var $: any;


@Component({
  selector: 'app-persons-details',
  templateUrl: './person-details.component.html',
  styleUrls: ['./person-details.component.scss']
})
export class PersonsDetailsComponent implements OnInit, OnDestroy {

  person: any;
  primary : any = {} ; 
  showAudit: boolean;
  constructor(public dataservice: personsDataService,
              public location: Location, 
              public showNotif : ShowNotification,
              private router: Router) {}

  ngOnInit() {
    this.person = this.dataservice.selectedPerson;
    if(!this.person) {
      this.showNotif.showNotification('bottom','center', 'warning', ' we will redirect you to the clients list to select an item in order to see his details')
      setTimeout(() => {
        this.router.navigateByUrl('/persons');
      }, 2000);
    }else{
        for (var char of this.person.emails) {
          if(char.stillPrimary)
            this.primary.email = char;
        }
        for (var char of this.person.phones) {
          if(char.stillPrimary)
            this.primary.phone = char;
        }
        for (var char of this.person.documents) {
          if(char.valid)
            this.primary.document = char;
        }
        var  a = new Array();
        for (var char of this.person.classification) {           
          a.push(char.type);
        }
        this.primary.classification = a.join(", ");
        
      //console.log('======= primary object =======',this.primary);   
    }
    //console.log("$$$$$$",this.person);
  }

  ngOnDestroy() { 
  }

  ShowAudit(){ 
    if(this.showAudit)
      this.showAudit =false ;
    else 
      this.showAudit = true ; 
  }
  goBack(): void {
    window.location.replace('');
  }


  

}
