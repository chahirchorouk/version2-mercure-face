import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { PersonsComponent } from './persons/persons.component';
import { PersonsDetailsComponent } from './persons-details/person-details.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SearchPersonsComponent } from './search-persons/search-persons.component';
import { PersonEditDetailsComponent } from './person-edit-details/person-edit-details.component';
import { SuiteEditDetailsComponent } from './suite-edit-details/suite-edit-details.component'
import { DashboardClientComponent } from './dashboard-client/dashboard-client.component';
import { OpprtunityComponent } from './opprtunity/opprtunity.component';






const routes: Routes = [
  { path: 'persons', component: PersonsComponent },
  { path: 'person-details', component: PersonsDetailsComponent },
  { path: 'dashboard-client', component: DashboardClientComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'command-client', component: SearchPersonsComponent },
  { path: 'person-edit', component: PersonEditDetailsComponent },
  { path: 'person-profil-edit', component: SuiteEditDetailsComponent },
  { path: 'opportunity', component: OpprtunityComponent },
  { path: '', redirectTo: 'persons', pathMatch: 'full' }

];

// import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';

// const routes: Routes =[
//   {
//     path: '',
//     redirectTo: 'dashboard',
//     pathMatch: 'full',
//   }, {
//     path: '',
//     component: AdminLayoutComponent,
//     children: [
//         {
//       path: '',
//       loadChildren: './layouts/admin-layout/admin-layout.module#AdminLayoutModule'
//   }]}
// { path: 'dashboard',      component: DashboardComponent },
// { path: 'user-profile',   component: UserProfileComponent },
// { path: 'table-list',     component: TableListComponent },
// { path: 'typography',     component: TypographyComponent },
// { path: 'icons',          component: IconsComponent },
// { path: 'maps',           component: MapsComponent },
// { path: 'notifications',  component: NotificationsComponent },
// { path: 'upgrade',        component: UpgradeComponent },
// { path: '',               redirectTo: 'dashboard', pathMatch: 'full' }
// ];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
