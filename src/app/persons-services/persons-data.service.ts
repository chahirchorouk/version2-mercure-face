import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
 
import 'rxjs/add/operator/toPromise';
import { Person } from './Person';
 
@Injectable()
export class personsDataService {
 
  private PersonsUrl = '/api/persons';  // URL to web API
  private headers = new Headers({'Content-Type': 'application/json'});
  selectedPerson: any;
 
  constructor(private http: Http) {}
 
  // Get all customers
  getPersons(): Promise<Person[]> {
    return this.http.get(this.PersonsUrl)
      .toPromise()
      .then(response => response.json() as Person[])
      .catch(this.handleError);
  }
  getPersonsPerPages(page:number,size:number):Promise<any> {
    const url = `/api/allPersons?page=${page}&size=${size}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as Person[])
      .catch(this.handleError);
  }
  
  getPersonsByName(id: number): Promise<Person[]> {
    const url = `${this.PersonsUrl}/${id}`;//`legalpersons/${id}`
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as Person)
      .catch(this.handleError);
  }
  
  getPersonsByClassif(classif:any,page:number,size:number): Promise<any> {
    const url = `/api/personsType?type=${classif}&page=${page}&size=${size}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as Person)
      .catch(this.handleError);
  }
  getPersonsByOppor(page:number,size:number): Promise<any> {
    const url = `/api/personsOppor?page=${page}&size=${size}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as Person)
      .catch(this.handleError);
  }
  getPersonsValid(bool:boolean,page:number,size:number): Promise<any> {
    const url = `/api/getValid?bool=${bool}&page=${page}&size=${size}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as Person)
      .catch(this.handleError);
  }
  getPersonsByActivity(activity: any,page:number,size:number): Promise<any> {
    const url = `/api/getByActivities?type=${activity}&page=${page}&size=${size}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as Person)
      .catch(this.handleError);
  } 
  delete(id: number): Promise<void> {
    const url = `${this.PersonsUrl}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  } 
  getPersonTypes(): Promise<any> {
    const url = `/api/getPersonType`;
    return this.http.get(url, {headers: this.headers})
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }
  getLegalForm(): Promise<any> {
    const url = `/api/getLegalForm`;
    return this.http.get(url, {headers: this.headers})
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }
  getClassificationTypes(): Promise<any> {
    const url = `/api/getClassifType`;
    return this.http.get(url, {headers: this.headers})
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }
  getActivityTypes(): Promise<any> {
    const url = `/api/getActivities`;
    return this.http.get(url, {headers: this.headers})
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }
  private handleError(error: any): Promise<any> {
    console.error('Error', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}