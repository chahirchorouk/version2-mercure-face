declare var $: any;

export class ShowNotification {
    showNotification(from, align, type, message) {
        $.notify({
            icon: 'notifications',
            message: message

        }, {
                type: type,
                timer: 4000,
                placement: {
                    from: from,
                    align: align
                }
            });
    }
}
