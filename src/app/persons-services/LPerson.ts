export class LPerson {
	public id: number;
	public name: string;
    public legalForm: string;
	public socialReason: string;
	public created: string;
	public activity: string; 	
	public capital: number; 
	public capitalCurr: string;
}