import {
  Injectable
} from '@angular/core';
import {
  Headers,
  Http
} from '@angular/http';

import 'rxjs/add/operator/toPromise';

import {
  LPerson
} from './LPerson';
import {
  Person
} from './Person';

@Injectable()
// tslint:disable-next-line:class-name
export class legalPersonsDataService {

  private lPersonsUrl = '/api/legalpersons'; // URL to web API
  private headers = new Headers({
    'Content-Type': 'application/json'
  });

  constructor(private http: Http) { }

  // Get all customers
  getLpersons(): Promise<LPerson[]> {
    return this.http.get(this.lPersonsUrl)
      .toPromise()
      .then(response => response.json() as LPerson[])
      .catch(this.handleError);
  }

  getLPersonsPerPages(page: number, size: number): Promise<any> {
    const url = `/api/legalAllPersons?page=${page}&size=${size}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as LPerson[])
      .catch(this.handleError);
  }
  getLPersonsPerCapital(min: number, max: number, page: number, size: number): Promise<any> {
    const url = `/api/personsCArange?min=${min}&max=${max}&page=${page}&size=${size}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as Person[])
      .catch(this.handleError);
  }
  getLpersonsById(id: number): Promise<LPerson[]> {
    const url = `${this.lPersonsUrl}/${id}`; // `legalpersons/${id}`
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as LPerson)
      .catch(this.handleError);
  }

  create(person: LPerson): Promise<LPerson> {
    return this.http
      .post(this.lPersonsUrl, JSON.stringify(person), {
        headers: this.headers
      })
      .toPromise()
      .then(res => res.json() as LPerson)
      .catch(this.handleError);
  }

  update(person: any): Promise<any> {
    const url = `/api/updateLegalPersons/${person.id}`; // `legalpersons/${id}`
    return this.http
      .put(url, JSON.stringify(person), {
        headers: this.headers
      })
      .toPromise()
      // .then(res => {if(res) res.json()} )
      .catch(this.handleError);
  }
  delete(id: number): Promise<void> {
    const url = `${this.lPersonsUrl}/${id}`;
    return this.http.delete(url, {
      headers: this.headers
    })
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('Error', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
