import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
 
import 'rxjs/add/operator/toPromise';
 
import { NPerson} from './NPerson';
 
@Injectable()
export class naturalPersonsDataService {
 
  private nPersonsUrl = '/api/naturalpersons';  // URL to web API
  private headers = new Headers({'Content-Type': 'application/json'});
 
  constructor(private http: Http) {}
 
  // Get all customers
  getNpersons(): Promise<NPerson[]> {
    return this.http.get(this.nPersonsUrl)
      .toPromise()
      .then(response => response.json() as NPerson[])
      .catch(this.handleError);
  }
  getNPersonsPerPages(page:number,size:number):Promise<any> {
      const url = `/api/naturalAllPersons?page=${page}&size=${size}`;
      return this.http.get(url)
        .toPromise()
        .then(response => response.json() as NPerson[])
        .catch(this.handleError);
  }
  getNpersonsById(id: number): Promise<NPerson[]> {
    const url = `${this.nPersonsUrl}/${id}`;//`legalpersons/${id}`
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as NPerson)
      .catch(this.handleError);
  }
 
  create(person: NPerson): Promise<NPerson> {
    return this.http
      .post(this.nPersonsUrl, JSON.stringify(person), {headers : this.headers})
      .toPromise()
      .then(res => {if(res){res.json() as NPerson}})
      .catch(this.handleError);
  }
  update(person: any): Promise<any> {
    const url = `/api/updateNaturalPersons/${person.id}`;//`legalpersons/${id}`
    return this.http
      .put(url, JSON.stringify(person), {headers : this.headers})
      .toPromise()
      //.then( res => {if(res) res.json()})
      .catch(this.handleError);
  }
 
  delete(id: number): Promise<void> {
    const url = `${this.nPersonsUrl}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }
  
  
  private handleError(error: any): Promise<any> {
    console.error('Error', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}