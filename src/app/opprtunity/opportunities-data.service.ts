import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Opportunity } from './opportunity';

import 'rxjs/add/operator/toPromise';


@Injectable()
export class OpportunityDataService {

  private lPersonsUrl = '/api/getOpportunities';  // URL to web API
  private headers = new Headers({ 'Content-Type': 'application/json' });

  constructor(private http: Http) { }

  // Get Opportunities by person Id 
  getOpportunitiesByPerson(id: number, page: number, size: number): Promise<any> {
    //const url = `${this.lPersonsUrl}?id=${id}`;
    let promise = new Promise((resolve, reject) => {
      let apiURL = `${this.lPersonsUrl}?id=${id}&page=${page}&size=${size}`;
      this.http.get(apiURL)
        .toPromise()
        .then(
          res => { // Success
            console.log('opportunity by person ---', res.json());
            resolve(res.json());
          }
        );
    });
    return promise;

  }
  getOpportunities(page: number, size: number): Promise<any> {
    //const url = `${this.lPersonsUrl}?id=${id}`;
    let promise = new Promise((resolve, reject) => {
      let apiURL = `${this.lPersonsUrl}?page=${page}&size=${size}`;
      this.http.get(apiURL)
        .toPromise()
        .then(
          res => { // Success
            console.log('opportunity all ---', res.json());
            resolve(res.json());
          }
        );
    });
    return promise;

  }
  add(Opportunity: any, id: number): Promise<any> {
    const url = `/api/addOpportunities?id=${id}`;
    return this.http
      .post(url, JSON.stringify(Opportunity), { headers: this.headers })
      .toPromise()
      //.then( res => {if(res) res.json()})
      .catch(this.handleError);
  }
  update(Opportunity: any, id: number): Promise<any> {
    const url = `/api/addOpportunities?id=${id}`;
    return this.http
      .put(url, JSON.stringify(Opportunity), { headers: this.headers })
      .toPromise()
      //.then( res => {if(res) res.json()})
      .catch(this.handleError);
  }
  // getPersonsByName(id: number): Promise<Person[]> {
  //   const url = `${this.PersonsUrl}/${id}`;//`legalpersons/${id}`
  //   return this.http.get(url)
  //     .toPromise()
  //     .then(response => response.json() as Person)
  //     .catch(this.handleError);
  // }

  private handleError(error: any): Promise<any> {
    console.error('Error', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}