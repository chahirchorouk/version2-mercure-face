import { Component, OnInit } from '@angular/core';
import { Person } from '../persons-services/Person';
import { personsDataService } from '../persons-services/persons-data.service';
import { OpportunityDataService } from './opportunities-data.service';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-opprtunity',
  templateUrl: './opprtunity.component.html',
  styleUrls: ['./opprtunity.component.scss']
})
export class OpprtunityComponent implements OnInit {
  pageSize: number = 5; //set the page elements size 
  pager: any = {};// pager object 
  pagedItems: any[];// paged items
  person: any;
  opportunities: any;
  addOpporBool = false;
  successSubmitForm = false;
  successUpdateForm = false;




  //to test 
  rForm: FormGroup;
  post: any;                     // A property for our submitted form
  description: string = '';
  name: string = '';
  source: string;
  closedate: string;
  campaign: string;
  status: string;
  forecast: string;
  probability: number;
  expectedIncome: number;

  // end test
  constructor(public dataservice: personsDataService, public opportunityDataService: OpportunityDataService, private fb: FormBuilder) {
    this.rForm = fb.group({
      'name': [null, Validators.required],
      'source': [null, Validators.required],
      'closeDate': [null, Validators.required],
      'campaign': [null, Validators.required],
      'status': [null, Validators.required],
      'forecast': [null, Validators.required],
      'probability': [null, Validators.required],
      'expectedIncome': [null, Validators.required],
      'description': [null, Validators.compose([Validators.required, Validators.minLength(30), Validators.maxLength(500)])],
      // 'validate': ''
    });

  }

  ngOnInit() {
    this.person = this.dataservice.selectedPerson;
    if (this.person) {
      this.setPage(1);
    }
    // this.opportunityDataService.getOpportunitiesByPerson(this.person.id).then(function (value) {
    //   //SUCCESS
    //   console.log(value);
    //   self.opportunities = value;
    // }, function (error) {
    //   console.log(error);
    // })

  }
  addPost(post) {


    console.log("----- post form add opportunity ", post);
    this.opportunityDataService.add(post, this.person.id).then(value => {
      console.log('value after response from insert opportunity ', value);
      if (value.ok == true && value.status == 200) {
        setTimeout(() => {
          this.successSubmitForm = true;
        }, 5000)
        this.rForm.reset();
        this.setPage(1);
      }
    }
    );
    //add person-id
  }
  updatePost(post) {
    console.log("----- post form update opportunity ", post);
    this.opportunityDataService.update(post, post.id).then(value => {
      console.log('value after response from insert opportunity ', value);
      if (value.ok == true && value.status == 200) {
        setTimeout(() => {
          this.successUpdateForm = true;
        }, 5000)

        this.setPage(1);
      }
    }
    );
    //add person-id
  }
  setPage(page: number) {
    var currentPage = 1;
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    if (this.pager.currentPage !== undefined) {
      currentPage = page;
      console.log("--------- current -----------: ", currentPage)
    }
    var self = this;
    this.opportunityDataService.getOpportunitiesByPerson(this.person.id, page - 1, this.pageSize).then(function (personsInf) {
      //SUCCESS
      self.pager = personsInf;
      self.pager.currentPage = currentPage;
      self.pager.pages = Array(personsInf.totalPages).fill(1);
      self.opportunities = personsInf.content;
      //console.log("pager opportunities", personsInf);
    }, function (error) {
      console.log(error);
    })

  }
  addOpportunity() {
    this.addOpporBool = !this.addOpporBool;
  }


}
