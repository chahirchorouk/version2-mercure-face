import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpprtunityComponent } from './opprtunity.component';

describe('OpprtunityComponent', () => {
  let component: OpprtunityComponent;
  let fixture: ComponentFixture<OpprtunityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpprtunityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpprtunityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
