import { Person } from "app/persons-services/Person";

export class Opportunity {
	public id: number;
	public name: string;
	public description: string;
	public person: Person;
}