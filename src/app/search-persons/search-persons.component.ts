import { Component, ViewChild} from '@angular/core';
import { personsDataService } from '../persons-services/persons-data.service';
import { legalPersonsDataService} from '../persons-services/legal-persons-data.service';
import { naturalPersonsDataService } from '../persons-services/natural-persons-data.service';
import { ShowNotification} from '../persons-services/showNotification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search-persons',
  templateUrl: './search-persons.component.html',
  styleUrls: ['./search-persons.component.scss']
})
export class SearchPersonsComponent  {
	person : any ; 
  constructor(public dataservice: personsDataService,    
              private NdataService: naturalPersonsDataService,
              private LdataService: legalPersonsDataService,
              public showNotif : ShowNotification,
              private router: Router){}

  ngOnInit() {
    this.person = this.dataservice.selectedPerson;
    if(!this.person) {
      this.showNotif.showNotification('bottom','center', 'warning', 'we will redirect you to the clients list to select an item in order to see his details')
      setTimeout(() => {
        this.router.navigateByUrl('/persons');
      }, 0);
    }
   }
  
}



