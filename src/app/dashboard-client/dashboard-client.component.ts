import { Component, OnInit } from '@angular/core';
import { personsDataService } from '../persons-services/persons-data.service';
import { legalPersonsDataService} from '../persons-services/legal-persons-data.service';
import { naturalPersonsDataService } from '../persons-services/natural-persons-data.service';
import { ShowNotification} from '../persons-services/showNotification.service';
import { Router } from '@angular/router';
import * as Chartist from 'chartist';
import Chart from 'chart.js';

@Component({
  selector: 'app-dashboard-client',
  templateUrl: './dashboard-client.component.html',
  styleUrls: ['./dashboard-client.component.scss']
})
export class DashboardClientComponent implements OnInit {
person: any;
// lineChart the first one for booking 
  public lineChartData:any[] = [
    {data:[65, 59, 80, 81, 56, 55, 40, 28, 65, 40, 19, 86],label: 'Hotel'},
    {data:[45, 48, 60, 29, 56, 17, 70, 54, 10, 87, 73, 17],label: 'Air'},
    {data:[28, 65, 40, 19, 86, 27, 60, 80, 81, 56, 55, 40],label: 'VO'},
    {data:[49, 54, 10, 87, 73, 17, 30, 48, 60, 29, 56, 17],label: 'CLub Med'}
  ];
  public lineChartLabels:Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July','August','September','October','November','December'];
  public lineChartType:string = 'line';
  public pieChartType:string = 'pie';
 
  // Pie
  public pieChartLabels:string[] = ['Air','Hotel Maroc','Hotel Externe','VO','Omra','Club Med'];
  public pieChartData:number[] = [30, 10,20,25,10,5];
 
  public randomizeType():void {
    this.lineChartType = this.lineChartType === 'line' ? 'bar' : 'line';
    this.pieChartType = this.pieChartType === 'doughnut' ? 'pie' : 'doughnut';
  }
 
  public chartClicked(e:any):void {
    console.log(e);
  }
 
  public chartHovered(e:any):void {
    console.log(e);
  }
// lineChart the second one service request 
  public lineChartData2:Array<any> = [
     {data: [65, 59, 80, 81, 56, 55, 40, 27, 40, 59, 80, 81,], label: 'Opened'},
    {data: [28, 48, 40, 19, 36, 27, 90, 48, 77, 9, 10, 27], label: 'Closed'},
    {data: [18, 48, 77, 9, 10, 27, 40, 59, 80, 81, 56, 55,], label: 'Canceled'}
  ];
  public lineChartLabels2:Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July','August','September','October','November','December'];
  public lineChartOptions:any = {
    responsive: true
  };
  public lineChartColors:Array<any> = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend:boolean = true;
  public lineChartType2:string = 'line';
 
  public randomize():void {
    let _lineChartData:Array<any> = new Array(this.lineChartData2.length);
    for (let i = 0; i < this.lineChartData2.length; i++) {
      _lineChartData[i] = {data: new Array(this.lineChartData2[i].data.length), label: this.lineChartData2[i].label};
      for (let j = 0; j < this.lineChartData2[i].data.length; j++) {
        _lineChartData[i].data[j] = Math.floor((Math.random() * 100) + 1);
      }
    }
    this.lineChartData2 = _lineChartData;
  }
 
  // events
  public chartClicked2(e:any):void {
    console.log(e);
  }
 
  public chartHovered2(e:any):void {
    console.log(e);
  }

// marketing graph 
// Doughnut
  public doughnutChartLabels:string[] = ['Emailing', 'TV', 'Radio','Sms'];
  public doughnutChartData:number[] = [45, 30, 15, 10];
  public doughnutChartType:string = 'doughnut';

// old chart library 
  startAnimationForLineChart(chart){
      let seq: any, delays: any, durations: any;
      seq = 0;
      delays = 80;
      durations = 500;

      chart.on('draw', function(data) {
        if(data.type === 'line' || data.type === 'area') {
          data.element.animate({
            d: {
              begin: 600,
              dur: 700,
              from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
              to: data.path.clone().stringify(),
              easing: Chartist.Svg.Easing.easeOutQuint
            }
          });
        } else if(data.type === 'point') {
              seq++;
              data.element.animate({
                opacity: {
                  begin: seq * delays,
                  dur: durations,
                  from: 0,
                  to: 1,
                  easing: 'ease'
                }
              });
          }
      });

      seq = 0;
  };
  startAnimationForBarChart(chart){
      let seq2: any, delays2: any, durations2: any;

      seq2 = 0;
      delays2 = 80;
      durations2 = 500;
      chart.on('draw', function(data) {
        if(data.type === 'bar'){
            seq2++;
            data.element.animate({
              opacity: {
                begin: seq2 * delays2,
                dur: durations2,
                from: 0,
                to: 1,
                easing: 'ease'
              }
            });
        }
      });

      seq2 = 0;
  };
  startAnimationForPieChart(chart){
      chart.on('draw',function(data){
      	if(data.type === 'slice'){
      		var pathLength = data.element._node.getTotalLength();
      		data.element.attr({
      			'stroke-dasharray': pathLength + 'px' + pathLength + 'px'
      		});
      		var animationDefinition = {
      			'stroke-dasharray': {
      				id: 'anim' + data.index,
      				dur : 1000,
      				from: -pathLength + 'px',
      				to : '0px',
      				easing : Chartist.Svg.Easing.easeOutQuint,
      				fill : 'freeze',
      				begin:''
      			}
      		};
      		if(data.index !== 0){
      			animationDefinition['stroke-dasharray'].begin = 'anim' +
      			(data.index - 1 ) + '.end';
      		}
      		data.element.attr({
      			'stroke-dashoffset': -pathLength + 'px'
      		});

      		data.element.animate(animationDefinition, false ); 
      	}
    });
  };
  constructor(public dataservice: personsDataService,    
              private NdataService: naturalPersonsDataService,
              private LdataService: legalPersonsDataService,
              public showNotif : ShowNotification,
              private router: Router) { }

  ngOnInit() {
    
  	this.person = this.dataservice.selectedPerson;
  	if(!this.person) {
      this.showNotif.showNotification('bottom','center', 'warning', 'we will redirect you to the clients list to select an item in order to see his details')
      setTimeout(() => {
        console.log('i m inside the setTime out ');
        this.router.navigateByUrl('/persons');
      }, 0);
    }else {
      this.randomizeType();
      this.randomize();
    	const dataDailySalesChart: any = {
          labels: ['M', 'T', 'W', 'T', 'F', 'S', 'S'],
          series: [
              [12, 17, 7, 17, 23, 18, 38]
          ]
      };

      const optionsDailySalesChart: any = {
          lineSmooth: Chartist.Interpolation.cardinal({
              tension: 0
          }),
          low: 0,
          high: 50, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
          chartPadding: { top: 0, right: 0, bottom: 0, left: 0},
      }

      var dailySalesChart = new Chartist.Line('#dailySalesChart', dataDailySalesChart, optionsDailySalesChart);

      this.startAnimationForLineChart(dailySalesChart);


      /* ----------==========     Completed Tasks Chart initialization    ==========---------- */

      const dataCompletedTasksChart: any = {
          labels: ['12am', '3pm', '6pm', '9pm', '12pm', '3am', '6am', '9am'],
          series: [
              [230, 750, 450, 300, 280, 240, 200, 190]
          ]
      };

      const optionsCompletedTasksChart: any = {
          lineSmooth: Chartist.Interpolation.cardinal({
              tension: 0
          }),
          low: 0,
          high: 1000, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
          chartPadding: { top: 0, right: 0, bottom: 0, left: 0}
      }

      var completedTasksChart = new Chartist.Line('#completedTasksChart', dataCompletedTasksChart, optionsCompletedTasksChart);

      // start animation for the Completed Tasks Chart - Line Chart
      this.startAnimationForLineChart(completedTasksChart);



      /* ----------==========     Emails Subscription Chart initialization    ==========---------- */

      var dataEmailsSubscriptionChart = {
        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        series: [
          [542, 443, 320, 780, 553, 453, 326, 434, 568, 610, 756, 895]

        ]
      };
      var optionsEmailsSubscriptionChart = {
          axisX: {
              showGrid: false
          },
          low: 0,
          high: 1000,
          chartPadding: { top: 0, right: 5, bottom: 0, left: 0}
      };
      var responsiveOptions: any[] = [
        ['screen and (max-width: 640px)', {
          seriesBarDistance: 5,
          axisX: {
            labelInterpolationFnc: function (value) {
              return value[0];
            }
          }
        }]
      ];
      var emailsSubscriptionChart = new Chartist.Bar('#emailsSubscriptionChart', dataEmailsSubscriptionChart, optionsEmailsSubscriptionChart, responsiveOptions);

      //start animation for the Emails Subscription Chart
      this.startAnimationForBarChart(emailsSubscriptionChart);     

    }
  }

}
